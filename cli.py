import argparse
from preprocessing.preprocessing import Preprocessing


def create_parser():
    prog_name = "python split_dataset.py"
    description = "Get source dataset, directory dataset and performs recoding"
    parser = argparse.ArgumentParser(prog=prog_name,
                                     description=description)
    parser.add_argument("-s", "--source",
                        help="path for the source file of the training dataset")
    parser.add_argument("--test", default=None,
                        help="path for the test file of the testing dataset")
    parser.add_argument("-d", "--destination",
                        help="destination directory of the dataset")
    parser.add_argument("--variable",
                        default=None,
                        help="y variable to use in train_test_split")
    parser.add_argument("--recoding", action="store_true",
                        default=False,
                        help="recoding categorical variables")
    parser.add_argument("--recoding_index", nargs='+', 
                        default=[],
                        help="Index of categorical variables to recode")
    parser.add_argument("--methods", nargs='+', type=str,
                        help="List of machine learning methods to apply")
    parser.add_argument("--measures", nargs='+', type=str,
                        help="Measurement methods")
    parser.add_argument("--folds", type=int, default=5,
                        help="folds number")
    parser.add_argument("--scale", type=bool, default=True,
                        help="should we scale data")
    parser.add_argument("--ignore", nargs='+', default=[],
                        help="columns to ignore")
    return parser


def check_arguments(dico_args):
    if dico_args['source'] is None:
        raise ValueError("You must provide a source directory")
    if dico_args["destination"] is None:
        raise ValueError("You must provide a destination directory")
    if dico_args["variable"] is None:
        raise ValueError("You must provide an output variable")

    
if __name__ == "__main__":
    parser = create_parser()
    args = parser.parse_args()
    check_arguments(vars(args))
    print(vars(args))

    p1 = Preprocessing(dataset_location=args.source,
                  test_path=args.test,
                  new_location=args.destination,
                  variable=args.variable,
                  tasks=args.methods,
                  measures=args.measures,
                  folds=args.folds)

    p1.save_new_dataset(recoding=args.recoding,
                        recoding_index=args.recoding_index,
                        scale = True,
                        ignore = args.ignore)
    p1.write_config()

    