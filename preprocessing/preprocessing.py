"""
Preprocess class
"""
import os
import pandas as pd
import numpy as np
import json
import itertools

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import StandardScaler


class Preprocessing():

    def __init__(self, dataset_location, test_path, new_location, variable,
                  tasks, measures, folds):
        """
        :param str dataset_location: original dataset location.
        :param str new_location: directory to store the new dataset.
        :param str variable: variable to predict.
        :param list tasks: lists of tasks.
        :param list measures: lists of measures.
        :param int folds: number of folds for cross validation.
                """

        if not isinstance(dataset_location, str):
            raise ValueError("dataset_location must be a string")
        if not isinstance(variable, str):
            raise ValueError("variable must be a string")
        if not isinstance(new_location, str):
            raise ValueError("new_location must be a string")

        self.__dataset_location = dataset_location
        self.__test_path = test_path
        self.__new_location = new_location
        self.__variable = variable
        self.__folds = folds
        self.__tasks = tasks
        self.__measures = measures


    def __process_dataset(self, recoding, recoding_index, scale = True,
                          ignore = []):
        """Splits dataset_locations into train test
        output variables are apppended to the end of the
        data frame
        
        :parameter bool recoding: Boolean indicating whether to recode or not
        :parameter set recoding_index: index of variables to recode
        :parameter bool scale: scale the data.
        :parameter list ignore: list of variable to ignore.
        :returns: tuple of train, test dataset_locations.
        :rtype: tuple
        :raises ValueError: If dataset_location and variable are not strings
        """

        if self.__test_path is not None:
            train = pd.read_csv(self.__dataset_location)
            test = pd.read_csv(self.__test_path)
            X = pd.concat([train, test], axis = 0)
            X.reset_index(drop = True, inplace = True) # reset index
        else:
            X = pd.read_csv(self.__dataset_location)

        # Coarse preprocessing :

        # delete Nan row
        X = X.dropna(axis = 0, how = 'any')

        # response recoding
        if any(i in self.__tasks for i in 
               ['Binary_Classification', 'Multi_Classification', 'Clustering']):
            le = LabelEncoder()
            le.fit(X[self.__variable])
            X[self.__variable] = le.transform(X[self.__variable])
        
        # extract response variable
        y = X[self.__variable]
        X = X.drop(self.__variable, axis=1)

        # drop unused label
        if len(ignore) != 0:
            X.drop(labels = ignore, axis = 1, inplace = True)

        # extract indexes of columns containing
        # strings
        label_indexes = [i if X.dtypes[i].name in {'object', 'string'} else -1
                        for i in range(X.dtypes.shape[0])]
        if set(label_indexes) != {-1}:
            label_indexes = set(label_indexes)
            try:
                label_indexes.remove(-1)
            except KeyError:
                pass
            label_indexes = list(label_indexes)
            label_encoder = LabelEncoder()
            for idx in label_indexes:
                # recode each string into
                # integers
                encoded_idx = label_encoder.fit_transform(X.iloc[:, idx])
                X[X.columns[idx]] = encoded_idx
                # recoding_index contains index of
                # categorical variables to be recoded
                # with one hot encoding
        if recoding:
            # extracting numerical and categorical
            # variables
            recoding_index = recoding_index + label_indexes
            x_num = X.drop(X.columns[recoding_index], axis=1)
            x_nom = X.iloc[:, recoding_index]
            ohe = OneHotEncoder(handle_unknown="ignore")
            # recoding categorical variables using one hot
            # encoding
            ohe = ohe.fit_transform(x_nom)
            ohe_variables = pd.DataFrame.sparse.from_spmatrix(ohe)
            X = pd.concat([ohe_variables, x_num], axis=1)
        x_train, x_test, y_train, y_test = train_test_split(X, y,
                                                            test_size=0.2)
        if scale:
            # scale the datas
            scaler = StandardScaler()
            x_train = pd.DataFrame(scaler.fit_transform(x_train.values),
                columns = x_train.columns, index = x_train.index)
            x_test = pd.DataFrame(scaler.fit_transform(x_test.values),
                columns = x_test.columns, index = x_test.index)

        x_train = pd.concat([x_train, y_train], axis=1)
        x_test = pd.concat([x_test, y_test], axis=1)

        return x_train, x_test


    def save_new_dataset(self, recoding=False, recoding_index=[], scale = True,
                         ignore = []):
        """gets dataset fromm *original\_location*
        performs train/test split with evnetually recoding
        and saves both dataset to new location

        :param bool recoding: wheter to recode or not
        :param list recoding_index: index of columns to recode
        :parameter bool scale: scale the data.
        :returns: True
        :rtype: bool
        :raises ValueError: if original_location and new_location
        are not strings or if they don't exist
        """
        
        x_train, x_test = self.__process_dataset(recoding, recoding_index,
                                                  scale, ignore)
        ## geting file name
        dataset_name = os.path.dirname(self.__dataset_location)

        ## create name for new
        ## test and trains datasets
        dataset_train = self.__new_location + "/" + dataset_name + "_train.csv"
        dataset_test = self.__new_location + "/" + dataset_name + "_test.csv"

        try:
            os.makedirs(self.__new_location)
        except FileExistsError:
            # directory already exists
            pass

        ## save datasets
        try:
            x_train.to_csv(dataset_train, index = False)
            x_test.to_csv(dataset_test, index = False)
        except:
            raise

    def __get_config(self, task, measure):
        """produce a configuration dictionnary 

        Args:
            tasks (list): List of ml methods.
            measures (list): List of measures methods.
            folds (int): number of folds for cross validation.

        Raises:
            Exception: measurement type doesn't exist.

        Returns:
            dict: configuration dictionnary
        """
        with open('models.json', 'r') as models:
            tasks_dic = json.load(models)
        try:
            mod_tokens = tasks_dic[task]
        except KeyError as err:
            print("{} isn't refer in the dictionnary".format(err))
            raise

        if measure != "pyRAPL" and measure != "codecarbon" \
            and measure is not None:
            raise Exception("{} measurement type doesn't exist".format(measure))
        
        config = {
            'name' : self.__new_location,
            'path' : "./datasets/" + self.__new_location + "/",
            'task' : task,
            'y' : self.__variable,
            'mod_tokens' : list(mod_tokens),
            'folds' : self.__folds,
            'measure' : measure
        }

        return config

    def write_config(self):
        """write a json file for each task and measure
        """

        for task, measure in itertools.product(self.__tasks, self.__measures):
            config = self.__get_config(task, measure)

            config_path = self.__new_location + "/" + task + "_" + measure + ".json"
            with open(config_path, 'w+') as config_file:
                json.dump(config, config_file, indent=2) #indent = 2 to add line breaks

