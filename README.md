# Datasets processing

Datasets pre-processing for greenml package.

## Install

You should install python dependences in the virtual environment manager of your choice. I personally use conda :

```bash
conda create -n datasets-env python=3.10
conda activate datasets-env
```

Then you can install all required libraries with the pip command line below:

```bash
pip install -r requirements.txt
```

## How it works

The command line interface takes one or more csv as input and then performs several pre-processing actions in order to get suitable data to provide as input into the [**pygreenml** package](https://gricad-gitlab.univ-grenoble-alpes.fr/ljk-greenml/py_green_ml).

This project also produce a json configuration file with the aim of getting all the parameter to supply to [**pygreenml**](https://gricad-gitlab.univ-grenoble-alpes.fr/ljk-greenml/py_green_ml) Runner class.

The prep-preocessing tasks are as follows :

- Variable recoding in several format for factor values.
- Random split to increase generality of data produced by [**pygreenml**](https://gricad-gitlab.univ-grenoble-alpes.fr/ljk-greenml/py_green_ml).
- Scaling of all predictors variables.

## Command line interface

The command line interface `cli.py` aim to launch preprocess on datasets located on the root of the package system file (detail about arguments below).

```
usage: python split_dataset.py [-h] [-s SOURCE] [--test TEST] [-d DESTINATION] [--variable VARIABLE] [--recoding] [--recoding_index RECODING_INDEX [RECODING_INDEX ...]]
                               [--methods METHODS [METHODS ...]] [--measures MEASURES [MEASURES ...]] [--folds FOLDS] [--scale SCALE]

Get source dataset, directory dataset and performs recoding

optional arguments:
  -h, --help            show this help message and exit
  -s SOURCE, --source SOURCE
                        path for the source file of the training dataset
  --test TEST           path for the test file of the testing dataset
  -d DESTINATION, --destination DESTINATION
                        destination directory of the dataset
  --variable VARIABLE   y variable to use in train_test_split
  --recoding            recoding categorical variables
  --recoding_index RECODING_INDEX [RECODING_INDEX ...]
                        Index of categorical variables to recode
  --methods METHODS [METHODS ...]
                        List of machine learning methods to apply
  --measures MEASURES [MEASURES ...]
                        Measurement methods
  --folds FOLDS         folds number
  --scale SCALE         should we scale data
```

As you cans see on the upper screenshot given by `python cli.py -h`. The cli script give the possibility to provide one csv or two csv files. This implemented features is mostly due to the nature of the raw data that we use, indeed in [Kaggle](https://www.kaggle.com/) data are generaly pre-processed in two csv train and test file.

example of use:

```bash
python cli.py --source concrete/concrete_data.csv --destination concrete --variable concrete_compressive_strength --methods Regression --measures pyRAPL
```

## Bibliography

- **pygreenml :** https://gricad-gitlab.univ-grenoble-alpes.fr/ljk-greenml/py_green_ml.
- **concrete data :** https://www.kaggle.com/datasets/elikplim/concrete-compressive-strength-data-set.
- **heart disease data:** https://www.kaggle.com/datasets/fedesoriano/heart-failure-prediction.
- **loan data :** https://www.kaggle.com/datasets/itssuru/loan-data
- **machines data :** ?source
- **mnist figures data :** https://www.kaggle.com/datasets/oddrationale/mnist-in-csv
- **abalone :** https://archive.ics.uci.edu/ml/datasets/Abalone